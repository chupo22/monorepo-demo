import { Injectable } from '@nestjs/common';
import { someCommon } from 'src/common';

@Injectable()
export class AppService {
  getHello(): string {
    return someCommon();
  }
}
