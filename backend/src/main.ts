enum EArgType {
  Foo = 'foo',
  Bar = 'bar',
}

export function getServiceName(): EArgType | undefined {
  const serviceName = process.argv[2];
  const available = Object.values(EArgType) as string[];

  if (!serviceName || !available.includes(serviceName)) {
    throw new Error(
      'Unexpected bootstrap arg. argv: ' + JSON.stringify(process.argv),
    );
  }

  return serviceName as EArgType;
}

async function bootstrap() {
  const serviceName = getServiceName();

  switch (serviceName) {
    // Migrations and debezium connector
    case EArgType.Foo: {
      const { bootstrap } = await import('./services/foo/main');
      await bootstrap();
      break;
    }
    case EArgType.Bar: {
      const { bootstrap } = await import('./services/bar/main');
      await bootstrap();
      break;
    }
  }
}

bootstrap();
